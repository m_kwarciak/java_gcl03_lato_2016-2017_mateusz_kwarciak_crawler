import LogowanieGUI.MenuLogowanie;

import java.awt.*;

/**
 * Created by Mateusz on 05.03.2017.
 */
public class Home {
    public static void main(String [] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MenuLogowanie();

            }
        });

    }
}
