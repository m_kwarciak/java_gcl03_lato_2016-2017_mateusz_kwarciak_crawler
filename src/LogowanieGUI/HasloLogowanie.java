package LogowanieGUI;

import javafx.scene.layout.Border;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mateusz on 05.03.2017.
 */
public class HasloLogowanie extends JFrame {
    public static JPasswordField robPoleHaslo(JPasswordField a)
    {
        JPasswordField wzor = a;

        wzor.setLocation(100,350);
        wzor.setVisible(true);
        wzor.setSize(250,30);
        wzor.setBackground(Color.WHITE);
        wzor.setForeground(Color.BLACK);
        wzor.setBorder(null);
        wzor.setFont(new Font("Tahoma", Font.PLAIN, 16));


        return  wzor;
    }
}
