package LogowanieGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mateusz on 06.03.2017.
 */
public class EtykietaHasloLogowanie extends JFrame {

    public static JLabel robEtykietaHasloLogowanie(JLabel a) {
        JLabel wzor = a;

        wzor.setLocation(100, 320);
        wzor.setSize(100, 30);
        wzor.setOpaque(true);
        wzor.setVisible(true);
        wzor.setForeground(Color.black);
        wzor.setBackground(Color.WHITE);
        wzor.setFont(new Font("Tahoma", Font.PLAIN, 14));

        return wzor;
    }
}