package LogowanieGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mateusz on 05.03.2017.
 */
public class PrzyciskLogowania extends JFrame {

    public static JButton robPrzycisk(JButton a){
        JButton wzor = a;

        wzor.setSize(250,75);
        wzor.setLocation(100,450);
        wzor.setForeground(Color.WHITE);
        wzor.setBackground(Color.GRAY);
        wzor.setBorderPainted(false);
        wzor.setFont(new Font("Tahoma", Font.PLAIN, 22));

        return wzor;
    }

}
