package LogowanieGUI;

import LogowanieGUI.HasloLogowanie;
import ProgramGUI.MenuProgram;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mateusz on 05.03.2017.
 */

public class MenuLogowanie extends JFrame implements ActionListener {
    public MenuLogowanie() {
        super("Logowanie");
        setSize(460,640);
        setLocation(750, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);

        JPanel panel = new JPanel(null);
        panel.setSize(460,640);
        panel.setBackground(Color.WHITE);
        panel.setVisible(true);


        JButton log = new JButton("ZALOGUJ");
        log.addActionListener(this);
        log= PrzyciskLogowania.robPrzycisk(log);
        panel.add(log);


        JPasswordField has = new JPasswordField();
        has = HasloLogowanie.robPoleHaslo(has);
        panel.add(has);

        JSeparator sep = new JSeparator(SwingConstants.HORIZONTAL);
        sep = SeparatorLogowanie.robSeparator(sep);
        panel.add(sep);

        JLabel lhas = new JLabel("Podaj haslo:");
        lhas = EtykietaHasloLogowanie.robEtykietaHasloLogowanie(lhas);
        panel.add(lhas);

        JTextField user = new JTextField();
        user = UzytkownikLogowanie.robUzytkownikLogowanie(user);
        panel.add(user);

        JSeparator sepuser = new JSeparator(SwingConstants.HORIZONTAL);
        sepuser = SeparatorLogowanieUser.robSeparatorLogowanieUser(sepuser);
        panel.add(sepuser);

        JLabel luser = new JLabel("Podaj nazwe uzytkownika: ");
        luser = EtykietaUserLogowanie.robEtykietaUserLogowanie(luser);
        panel.add(luser);


        add(panel);
        setVisible(true);






    }

    @Override
    public void actionPerformed(ActionEvent e) {

        dispose();
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
               new MenuProgram();

            }
        });

    }
}
