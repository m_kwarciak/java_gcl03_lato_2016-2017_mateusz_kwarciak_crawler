package LogowanieGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mateusz on 05.03.2017.
 */
public class SeparatorLogowanieUser extends JFrame {

    public static JSeparator robSeparatorLogowanieUser(JSeparator a)
    {
        JSeparator wzor = a;

        wzor.setLocation(100,285);
        wzor.setVisible(true);
        wzor.setSize(250,10);
        wzor.setForeground(Color.BLACK);

        return wzor;
    }
}
